package xyz.spacefox.todochecklist.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that represents login form request answer from JSON API.
 */
public class LoginResponse {

    @SerializedName("response")
    private int mResponseStatus;

    @SerializedName("session")
    private String mSessionId;

    public int getResponseStatus() {
        return mResponseStatus;
    }

    public String getSessionId() {
        return mSessionId;
    }
}
