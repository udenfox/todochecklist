package xyz.spacefox.todochecklist.api.rest;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import xyz.spacefox.todochecklist.api.model.LoginResponse;

/**
 * Interface to do API requests using retrofit
 */
public interface  ApiService {

    @POST("/")
    @FormUrlEncoded
    void attemptLogin(@FieldMap HashMap<String, String> loginData, Callback<LoginResponse> callback);

}
