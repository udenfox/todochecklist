package xyz.spacefox.todochecklist.api.rest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Deserializer class for GSON to properly deserialize date given from API to standard Java
 * {@link java.util.Date Date} object
 */
public class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        long date = element.getAsLong();
        if(date==0) {
            return null;
        }
        return new Date(date*1000);
    }
}
