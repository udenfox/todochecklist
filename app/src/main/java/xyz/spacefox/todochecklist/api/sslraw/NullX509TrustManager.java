package xyz.spacefox.todochecklist.api.sslraw;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * Trust manager to accept all ssl certificates
 */
public class NullX509TrustManager implements X509TrustManager {
    //TODO: Don't use OR Configure to approve only proper cert when availible.
    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
