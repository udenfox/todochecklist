package xyz.spacefox.todochecklist.api.sslraw;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import rpc.Message;

/**
 * Class that represents Raw ssl client to interact with server using protobuf packets.
 */
public class RawClient {
    private static final String LOG_TAG = "RawSllClient";

    private URI mURI;
    private Listener mListener;
    private Socket mSocket;
    private Boolean mConnected;
    private Thread mThread;
    private Handler mHandler;
    private HandlerThread mHandlerThread;
    private byte[] mSequent;

    // Lock for sending action synchronization
    private final Object mSendLock = new Object();

    // Data sizes in bytes
    private final int HEADER_SIZE = 8;
    private final int SEQUENT_SIZE = 8;
    private final int FLAGS_SIZE = 4;
    private final int DATA_SIZE = 4;

    /**
     * Default client constructor
     * @param uri URI to connect
     * @param listener Listener object to handle data exchange events
     */
    public RawClient(URI uri, Listener listener) {
        mURI = uri;
        mListener = listener;
        mConnected = false;
        mSequent = new byte[SEQUENT_SIZE];

        mHandlerThread = new HandlerThread("sslSocket-thread");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());

    }

    /**
     * Sets new listener to client
     * @param listener listener to set
     */
    public void setListener(Listener listener) {
        mListener = listener;
    }

    /**
     * Connects to a server and creates socket with input and output streams.
     * Starts reading input stream for new packets of data.
     */
    public void connect() {
        if (mThread != null && mThread.isAlive()){
            return;
        }

        mThread = new Thread(() -> {
            try {
                SocketFactory factory = getSSLSocketFactory();
                mSocket = factory.createSocket();
                mSocket.setKeepAlive(true);
                mSocket.connect(new InetSocketAddress(mURI.getHost(), mURI.getPort()), 5000);
                mSocket.setKeepAlive(true);

                mListener.onConnect();
                mConnected = true;

                InputStream str = mSocket.getInputStream();
                startListening(str);

            } catch (Exception ex) {
                mListener.onError(ex);
            }

        });

        mThread.start();
    }

    /**
     * Close connection to a socket.
     */
    public void disconnect(){
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mSocket != null) {
                    try {
                        mConnected = false;
                        mSocket.close();
                    } catch (IOException e) {
                        Log.d(LOG_TAG, "Error while disconnecting", e);
                        mListener.onError(e);
                    }
                    mSocket = null;
                }
                mListener.onDisconnect();
            }
        });
    }

    /**
     * @return current connection status
     */
    public boolean isConnected(){
        return mConnected;
    }

    /**
     * Starts reading socket's inputStream and parse data packet if available.
     */
    private void startListening(InputStream in) {
        while(mConnected) {

            try {

                if (in.available() == -1) {
                    disconnect();
                    break;
                }

                byte[] buff = new byte[SEQUENT_SIZE];
                int ii = in.read(buff, 0, SEQUENT_SIZE);

                if(ii>1){
                    mSequent = buff;
                    byte[] header = new byte[HEADER_SIZE];
                    in.read(header, 0, 8);

                    byte[] protoSizeBytes = Arrays.copyOfRange(header, 0, DATA_SIZE);
                    byte[] flags = Arrays.copyOfRange(header, DATA_SIZE, HEADER_SIZE);
                    int protoSize = ByteBuffer.wrap(protoSizeBytes).order(ByteOrder.LITTLE_ENDIAN)
                            .getInt();
                    if (protoSize > 0) {
                        byte[] dataProto = new byte[protoSize];
                        in.read(dataProto, 0, protoSize);
                        Message.Response rsp =  Message.Response.parseFrom(dataProto);
                        mListener.onReceive(flags, rsp);
                    } else {
                       throw new IOException();
                    }

                }

            } catch (IOException e){
                mListener.onError(e);
            }


        }
    }

    /**
     * Creates and sends data packet to a server.
     */
    public void send(Message.Request msgReq){

        byte[] sequent = mSequent;
        byte[] proto = msgReq.toByteArray();
        byte[] header = ByteBuffer
                .allocate(HEADER_SIZE)
                .order(ByteOrder.LITTLE_ENDIAN)
                .putLong(proto.length)
                .array();

        byte[] toSend = new byte[SEQUENT_SIZE + HEADER_SIZE + proto.length];

        ByteBuffer buf = ByteBuffer.wrap(toSend);
        buf.put(sequent);
        buf.put(header);
        buf.put(proto);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mSendLock) {
                    try {
                        OutputStream out = mSocket.getOutputStream();
                        out.write(toSend);
                        out.flush();
                    } catch (Exception e) {
                        mListener.onError(e);
                    }
                }
            }
        });
    }

    /**
     * Retrieves configured SSL socket factory object
     * @return configured ssl socket factory
     */
    private SSLSocketFactory getSSLSocketFactory() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, new TrustManager[] {new NullX509TrustManager()}, null);
        return context.getSocketFactory();
    }

    /**
     * Listener interface with callbacks to handle data exchange events
     */
    public interface Listener {
        void onConnect();
        void onReceive(byte[] flags, Message.Response response);
        void onError(Exception e);
        void onDisconnect();
    }
}
