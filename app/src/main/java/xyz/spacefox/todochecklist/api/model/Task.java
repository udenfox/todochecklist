package xyz.spacefox.todochecklist.api.model;

import android.database.Cursor;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import xyz.spacefox.todochecklist.utils.DBConsts;

/**
 * Class that represents Task API object
 */
public class Task {

    @SerializedName("notes")
    private String mNotes;

    @SerializedName("remindOnDate")
    private Date mRemindOnDate;

    @SerializedName("invitee")
    private String mInvitee;

    @SerializedName("remindOnLocation")
    private OnLocationInfo mRemindOnLocation;

    @SerializedName("alias")
    private String mAlias;

    @SerializedName("version")
    private String mVersion;

    @SerializedName("creationDate")
    private Date mCreationDate;

    @SerializedName("completed")
    private Boolean mCompleted;

    @SerializedName("title")
    private String mTitle;

    /**
     * Empty constructor
     */
    public Task() {
    }

    /**
     * Constructs Task object from cursor
     * @param c moved to necessary position cursor
     */
    public Task(Cursor c) {

        Double positionLat = c.getDouble(c.getColumnIndex(DBConsts.COL_REMIND_LAT));
        Double positionLng = c.getDouble(c.getColumnIndex(DBConsts.COL_REMIND_LNG));
        LatLng positionCoordinates;
        if (positionLat.equals(0.) && positionLng.equals(0.)) {
            positionCoordinates = null;
        } else {
            positionCoordinates = new LatLng(positionLat, positionLng);
        }
        boolean onEnter = c.getInt(c.getColumnIndex(DBConsts.COL_REMIND_ON_ENTER))!= 0;
        boolean completed = c.getInt(c.getColumnIndex(DBConsts.COL_COMPLETED))!= 0;
        long remindDateMillis = c.getLong(c.getColumnIndex(DBConsts.COL_REMIND_ON_DATE));
        long createDateMillis = c.getLong(c.getColumnIndex(DBConsts.COL_CREATION_DATE));

        Date remindDate = remindDateMillis > 0 ? new Date(remindDateMillis) : null;
        Date createDate = createDateMillis > 0 ? new Date(createDateMillis) :null;

        OnLocationInfo locationInfo = new OnLocationInfo();
        locationInfo.setCoordinates(positionCoordinates);
        locationInfo
                .setDescription(c.getString(c.getColumnIndex(DBConsts.COL_REMIND_DESCRIPTION)));
        locationInfo.setRadius(c.getInt(c.getColumnIndex(DBConsts.COL_REMIND_RADIUS)));
        locationInfo.setRemindOnEnter(onEnter);

        this.setAlias(c.getString(c.getColumnIndex(DBConsts.COL_TASK_ALIAS)));
        this.setTitle(c.getString(c.getColumnIndex(DBConsts.COL_TASK_TITLE)));
        this.setCompleted(completed);
        this.setRemindOnDate(remindDate);
        this.setNotes(c.getString(c.getColumnIndex(DBConsts.COL_NOTES)));
        this.setInvitee(c.getString(c.getColumnIndex(DBConsts.COL_INVITEE)));
        this.setRemindOnLocation(locationInfo);
        this.setVersion(c.getString(c.getColumnIndex(DBConsts.COL_VERSION)));
        this.setCreationDate(createDate);

    }

    public String getNotes() {
        return mNotes;
    }

    public Date getRemindOnDate() {
        return mRemindOnDate;
    }

    public String getInvitee() {
        return mInvitee;
    }

    public OnLocationInfo getRemindOnLocation() {
        return mRemindOnLocation;
    }

    public String getAlias() {
        return mAlias;
    }

    public String getVersion() {
        return mVersion;
    }

    public Date getCreationDate() {
        return mCreationDate;
    }

    public Boolean getCompleted() {
        return mCompleted;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setNotes(String notes) {
        mNotes = notes;
    }

    public void setRemindOnDate(Date remindOnDate) {
        mRemindOnDate = remindOnDate;
    }

    public void setInvitee(String invitee) {
        mInvitee = invitee;
    }

    public void setRemindOnLocation(OnLocationInfo remindOnLocation) {
        mRemindOnLocation = remindOnLocation;
    }

    public void setAlias(String alias) {
        mAlias = alias;
    }

    public void setVersion(String version) {
        mVersion = version;
    }

    public void setCreationDate(Date creationDate) {
        mCreationDate = creationDate;
    }

    public void setCompleted(Boolean completed) {
        mCompleted = completed;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    @Override
    public int hashCode() {
        return mAlias.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        Task other = (Task) o;
        if (!mAlias.equals(other.getAlias()))
            return false;
        return true;
    }
}
