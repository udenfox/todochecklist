package xyz.spacefox.todochecklist.api.rest;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Deserializer class for GSON to properly deserialize LatLng coordinates given from API to
 * {@link LatLng} object
 */
public class LatLngDeserializer implements JsonDeserializer<LatLng> {

    @Override
    public LatLng deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject coordinates = element.getAsJsonObject();

        Double lat = coordinates.get("latitude").getAsDouble();
        Double lng = coordinates.get("longitude").getAsDouble();

        if (lat.equals(0.) && lng.equals(0.)) {
            return null;
        }

        return new LatLng(lat, lng);
    }
}
