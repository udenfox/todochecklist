package xyz.spacefox.todochecklist.api.rest;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import xyz.spacefox.todochecklist.utils.Consts;

/**
 * Singleton class that represents API client using Retrofit.
 * Contains GSON configuration.
 */
public class RestClient {

    private static RestClient sInstance = new RestClient();

    private ApiService mApiService;
    private Gson mGson;
    private RestAdapter mRestAdapter;

    private RestClient() {

        mGson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .registerTypeAdapter(LatLng.class, new LatLngDeserializer())
                .create();

        //TODO: Set log level to non-verbose afrer release
        mRestAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Consts.API_URL)
                .setConverter(new GsonConverter(mGson))
                .build();

        mApiService = mRestAdapter.create(ApiService.class);
    }


    /** @return rest client instance */
    public static RestClient getInstance() {
        return sInstance;
    }

    /** @return Api service to interact with */
    public ApiService getApiService()
    {
        return mApiService;
    }

    /** @return  GSON object from Rest client */
    public Gson getGson() {
        return mGson;
    }

}
