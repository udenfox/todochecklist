package xyz.spacefox.todochecklist.api.model;

import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import xyz.spacefox.todochecklist.R;
import xyz.spacefox.todochecklist.utils.Consts;
import xyz.spacefox.todochecklist.utils.DBConsts;

/**
 * Class that represents Task list API object
 */
public class TaskList {

    @SerializedName("tasks")
    private ArrayList<String> mTasks;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("alias")
    private String mAlias;

    /**
     * Empty constructor
     */
    public TaskList() {
    }

    /**
     * Constructs TaskList object from cursor
     * @param c moved to necessary position cursor
     * @param ctx context to get default lists name from string resources
     */
    public TaskList(Cursor c, Context ctx) {
        String alias = "";
        alias = c.getString(c.getColumnIndex(DBConsts.COL_LIST_ALIAS));

        if (alias.equals(Consts.ALL_TASKS_ALIAS)) {
            this.setTitle(ctx.getString(R.string.all_tasks_list_title));
        } else if (alias.equals(Consts.NO_LIST_TASKS_ALIAS)) {
            this.setTitle(ctx.getString(R.string.no_list_list_title));
        } else {
            this.setTitle(c.getString(c.getColumnIndex(DBConsts.COL_LIST_TITLE)));
        }
        this.setAlias(alias);
    }

    public ArrayList<String> getTasks() {
        return mTasks;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getAlias() {
        return mAlias;
    }

    public void setTasks(ArrayList<String> tasks) {
        mTasks = tasks;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setAlias(String alias) {
        mAlias = alias;
    }

    @Override
    public int hashCode() {
        return mAlias.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        TaskList other = (TaskList) o;
        if (!mAlias.equals(other.getAlias()))
            return false;
        return true;
    }
}
