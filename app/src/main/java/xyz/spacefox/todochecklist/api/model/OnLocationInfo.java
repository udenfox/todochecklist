package xyz.spacefox.todochecklist.api.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Class that represents Task's Remind on location info
 */
public class OnLocationInfo {

    @SerializedName("coordinates")
    private LatLng mCoordinates;

    @SerializedName("onEnter")
    private Boolean mRemindOnEnter;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("radius")
    private int mRadius;

    public LatLng getCoordinates() {
        return mCoordinates;
    }

    public Boolean getRemindOnEnter() {
        return mRemindOnEnter;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getRadius() {
        return mRadius;
    }

    public void setCoordinates(LatLng coordinates) {
        mCoordinates = coordinates;
    }

    public void setRemindOnEnter(Boolean remindOnEnter) {
        mRemindOnEnter = remindOnEnter;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setRadius(int radius) {
        mRadius = radius;
    }

    @Override
    public int hashCode() {
        return mCoordinates.hashCode()+mRadius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        OnLocationInfo other = (OnLocationInfo) o;
        if (!mCoordinates.equals(other.getCoordinates()) || mRadius != other.getRadius())
            return false;
        return true;
    }
}
