package xyz.spacefox.todochecklist.api.model;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;

import java.util.HashMap;
import java.util.Map;

import xyz.spacefox.todochecklist.utils.Consts;
import xyz.spacefox.todochecklist.utils.Util;

/**
 * Class that represents request data fields for login API call.
 */
public class LoginRequestData {

    private String mAction;
    private String mService;
    private String mLogin;
    private String mPassword;
    private String mDeviceId;
    private String mDevice;
    private String mPlatform;
    private String mPlatformVersion;
    private String mAppVersion;
    private String mLocale;
    private String mTimezone;
    private Context mContext;


    /**
     * Default constructor to compile login data.
     * @param login user's login string
     * @param password user's password string
     * @param context context to get
     */
    public LoginRequestData(String login, String password, Context context) {
        this.mLogin = login;
        this.mPassword = password;
        this.mContext = context;

        this.mAction = Consts.LOGIN_ACTION;
        this.mService = Consts.LOGIN_SERVICE;
        this.mPlatform = Consts.PLATFORM;

        this.mPlatformVersion = Build.VERSION.RELEASE;
        this.mDeviceId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        this.mLocale = mContext.getResources().getConfiguration().locale.toString();
        this.mTimezone = Util.getTimeZone();
        this.mAppVersion = Util.getAppVersion(mContext);
        this.mDevice = Util.getHostName("null");

    }

    /**
     * Assembles login data to a Hashmap to use as filedMap in API call. Every value of map encoded
     * using base64.
     * @return Hashmap with encoded login data
     */
    public HashMap<String,String> getEncodedMap(){
        HashMap<String, String> outMap = new HashMap<>();
        outMap.put("action", mAction);
        outMap.put("service", mService);
        outMap.put("login", mLogin);
        outMap.put("password", mPassword);
        outMap.put("deviceid", mDeviceId);
        outMap.put("device", mDevice);
        outMap.put("platform", mPlatform);
        outMap.put("platformversion", mPlatformVersion);
        outMap.put("appversion", mAppVersion);
        outMap.put("locale", mLocale);
        outMap.put("timezone", mTimezone);

        for (Map.Entry<String, String> entry : outMap.entrySet()) {
            byte[] data = entry.getValue().getBytes();
            entry.setValue(Base64.encodeToString(data, Base64.DEFAULT));
        }

        return outMap;
    }
}
