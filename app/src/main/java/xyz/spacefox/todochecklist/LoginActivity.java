package xyz.spacefox.todochecklist;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import xyz.spacefox.todochecklist.api.model.LoginRequestData;
import xyz.spacefox.todochecklist.api.model.LoginResponse;
import xyz.spacefox.todochecklist.api.rest.RestClient;
import xyz.spacefox.todochecklist.utils.Consts;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private TextInputLayout mEmailTextInput;
    private TextInputLayout mPasswordTextInput;
    private View mProgressView;
    private View mLoginFormView;
    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mEmailTextInput = (TextInputLayout) findViewById(R.id.email_input);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordTextInput = (TextInputLayout) findViewById(R.id.password_input);
        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        mPrefs = getSharedPreferences(Consts.PREFS_NAME, MODE_PRIVATE);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(view -> attemptLogin());

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailTextInput.setError(null);
        mPasswordTextInput.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordTextInput.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordTextInput.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailTextInput.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailTextInput.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            makeLoginCall(email, password);
        }
    }

    /**
     * Makes login api call with specified login data
     * @param login user's login
     * @param password user's password
     */
    private void makeLoginCall(String login, String password) {

        showProgress(true);

        HashMap<String, String> out = new LoginRequestData(login, password, this).getEncodedMap();

        RestClient.getInstance().getApiService().attemptLogin(out, new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse loginResponse, Response response) {
                showProgress(false);
                if (loginResponse.getResponseStatus() == Consts.HTTP_STATUS_OK) {
                    mPrefs.edit().clear().apply();
                    mPrefs.edit().putString(Consts.PREFS_SESSIONID,
                            loginResponse.getSessionId()).apply();
                    mPrefs.edit().putString(Consts.PREFS_LOGIN, mEmailView.getText()
                            .toString()).apply();
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    LoginActivity.this.finish();
                } else {

                    // Show the "Login error" text without specifying the error case just because
                    // server returns 302 status if data is wrong.
                    Toast.makeText(LoginActivity.this, getString(R.string.login_error),
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                showProgress(false);
                Toast.makeText(LoginActivity.this, getString(R.string.error_network),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show?1:0).setListener(new AnimatorListenerAdapter(){
            @Override
            public void onAnimationEnd(Animator animation){
                mProgressView.setVisibility(show?View.VISIBLE:View.GONE);
            }
        });
    }

}

