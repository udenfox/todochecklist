package xyz.spacefox.todochecklist.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import xyz.spacefox.todochecklist.R;
import xyz.spacefox.todochecklist.api.model.Task;
import xyz.spacefox.todochecklist.utils.DBConsts;

/**
 * Subclass of {@link android.widget.CursorAdapter} which provides adapter to populate
 * tasks list items.
 */
public class TaskListCursorAdapter extends CursorAdapter {

    private LayoutInflater cursorInflater;

    public TaskListCursorAdapter(Context context, Cursor cursor, boolean autoRequery) {
        super(context, cursor, autoRequery);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.main_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor c) {
        TextView taskTitle = (TextView) view.findViewById(R.id.item_task_title);
        CheckBox completedCheck = (CheckBox) view.findViewById(R.id.item_task_check);
        LinearLayout remindBlock = (LinearLayout) view.findViewById(R.id.item_remind_block);
        TextView remindDateText = (TextView) view.findViewById(R.id.item_task_alarm_date);
        TextView remindTimeText = (TextView) view.findViewById(R.id.item_task_alarm_time);

        long remindMillis = c.getLong(c.getColumnIndex(DBConsts.COL_REMIND_ON_DATE));

        taskTitle.setText(c.getString(c.getColumnIndex(DBConsts.COL_TASK_TITLE)));
        if (remindMillis > 0) {
            remindBlock.setVisibility(View.VISIBLE);
            Date remindDate = new Date(remindMillis);

            remindDateText.setText(SimpleDateFormat.getDateInstance().format(remindDate));
            remindTimeText.setText(SimpleDateFormat.getTimeInstance(DateFormat.SHORT).format(remindDate));
        } else {
            remindBlock.setVisibility(View.GONE);
        }

        boolean completed = c.getInt(c.getColumnIndex(DBConsts.COL_COMPLETED)) != 0;
        if (completed) {
            completedCheck.setChecked(true);
            taskTitle.setPaintFlags(taskTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            taskTitle.setTextColor(ContextCompat.getColor(context, R.color.secondary_text));
        } else {
            completedCheck.setChecked(false);
            taskTitle.setPaintFlags(taskTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            taskTitle.setTextColor(ContextCompat.getColor(context, R.color.primary_text));
        }

    }

    /**
     * Retrieves an {@link Task} object collected from database at specified position.
     * @param position Position of item to get
     */
    public Task getItem(int position){
        Cursor c = getCursor();

        if(c.moveToPosition(position)){
            return new Task(c);

        }

        return null;

    }
}
