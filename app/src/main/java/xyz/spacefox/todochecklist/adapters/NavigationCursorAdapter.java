package xyz.spacefox.todochecklist.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import xyz.spacefox.todochecklist.R;
import xyz.spacefox.todochecklist.api.model.TaskList;
import xyz.spacefox.todochecklist.utils.Consts;
import xyz.spacefox.todochecklist.utils.DBConsts;

/**
 * Subclass of {@link android.widget.CursorAdapter} which provides adapter to populate
 * NavigationDrawer listview items.
 */
public class NavigationCursorAdapter extends CursorAdapter {

    private LayoutInflater cursorInflater;
    private Context ctx;

    public NavigationCursorAdapter(Context context, Cursor cursor, boolean autoRequery) {
        super(context, cursor, autoRequery);
        this.ctx = context;
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return cursorInflater.inflate(R.layout.nav_drawer_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView icon = (ImageView) view.findViewById(R.id.nav_drawer_item_icon);
        TextView listTitle = (TextView) view.findViewById(R.id.nav_drawer_item_text);

        String alias = cursor.getString(cursor.getColumnIndex(DBConsts.COL_LIST_ALIAS));

        switch (alias) {
            case Consts.ALL_TASKS_ALIAS:
                icon.setImageDrawable(ContextCompat
                        .getDrawable(context, R.drawable.ic_folder_multiple_grey600_24dp));
                listTitle.setText(context.getString(R.string.all_tasks_list_title));

                break;
            case Consts.NO_LIST_TASKS_ALIAS:
                icon.setImageDrawable(ContextCompat
                        .getDrawable(context, R.drawable.ic_folder_grey600_24dp));
                listTitle.setText(context.getString(R.string.no_list_list_title));

                break;
            default:
                icon.setImageDrawable(ContextCompat
                        .getDrawable(context, R.drawable.ic_view_list_grey600_24dp));
                listTitle.setText(cursor.getString(cursor.getColumnIndex(DBConsts.COL_LIST_TITLE)));
                break;
        }

    }

    /**
     * Retrieves an {@link TaskList} object collected from database at specified position.
     * @param position Position of item to get
     */
    public TaskList getItem(int position){
        Cursor c = getCursor();
        if(c.moveToPosition(position)){
            return new TaskList(c, ctx);
        }

        return null;

    }
}
