package xyz.spacefox.todochecklist.service;

/**
 * Enum to represent {@link ApiCallService} work results passed through broadcast intent
 */
public enum ServiceResult {
    UPDATE, ERROR
}
