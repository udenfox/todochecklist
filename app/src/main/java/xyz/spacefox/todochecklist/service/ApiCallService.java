package xyz.spacefox.todochecklist.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import rpc.Message;
import rpc.MessageCommon;
import rpc.MessageTypeOuterClass;
import rpc.MessageWorkgroup;
import xyz.spacefox.todochecklist.ToDoApplication;
import xyz.spacefox.todochecklist.api.model.Task;
import xyz.spacefox.todochecklist.api.model.TaskList;
import xyz.spacefox.todochecklist.api.rest.RestClient;
import xyz.spacefox.todochecklist.api.sslraw.RawClient;
import xyz.spacefox.todochecklist.utils.Consts;
import xyz.spacefox.todochecklist.utils.Database;

/**
 * Service performs SSL API call to get tasks from server.
 */
public class ApiCallService extends Service {

    private RawClient mClient;
    private URI mURI;
    private Database mDb;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mClient.connect();

        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mDb = ((ToDoApplication) getApplication()).getDatabaseHelper();;

        try {
            mURI =  new URI(Consts.SSL_URI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        mClient = new RawClient(mURI, new RawClient.Listener() {
            @Override
            public void onConnect() {
                sendMsg();
            }

            @Override
            public void onReceive(byte[] flags, Message.Response response) {
                if (response.getErrorCode() == Consts.HTTP_STATUS_OK) {
                    parseAndSave(response);
                    sendUpdateBroadcast(ServiceResult.UPDATE);
                } else {
                    sendUpdateBroadcast(ServiceResult.ERROR);
                }

            }

            @Override
            public void onError(Exception e) {
               e.printStackTrace();
                sendUpdateBroadcast(ServiceResult.ERROR);
            }

            @Override
            public void onDisconnect() {

            }
        });

    }

    @Override
    public void onDestroy() {

        if (mClient.isConnected()) {
            mClient.disconnect();
        }
        super.onDestroy();
    }

    /**
     * Composes and sends message to SSL raw client to get tasks
     */
    private void sendMsg() {
        SharedPreferences prefs = getSharedPreferences(Consts.PREFS_NAME, MODE_PRIVATE);

        MessageWorkgroup.WorkGroupsListRequest workgroup = MessageWorkgroup.WorkGroupsListRequest.newBuilder()
                .setSessionId(prefs.getString(Consts.PREFS_SESSIONID, "null"))
                .setFilter(MessageCommon.Filter.EXISTING_ONLY)
                .build();

        Message.Request rqs = Message.Request.newBuilder()
                .setIsDebug(true)
                .setMessageType(MessageTypeOuterClass.MessageType.RPC_WORKGROUPS_LIST)
                .setServiceType(1)
                .setWorkgroupsList(workgroup).build();

        mClient.send(rqs);
    }

    /**
     * Parses and saves data gathered from SSL response to a database
     * @param response SSL response message
     */
    private void parseAndSave(Message.Response response) {
        List<MessageCommon.WorkGroupInfo> lst = response.getWorkgroupsList().getWorkgroupInfoListList();

        mDb.clearDatabase();

        if (lst.size() > 0) {
            for (MessageCommon.WorkGroupInfo info : lst){

                switch (info.getWorkgroupType()) {
                    case Consts.TYPE_LIST:
                        mDb.addTaskList(RestClient.getInstance()
                                .getGson()
                                .fromJson(info.getWorkgroupMetadata(), TaskList.class));


                        break;
                    case Consts.TYPE_TASK:
                        mDb.addTask(RestClient.getInstance()
                                .getGson()
                                .fromJson(info.getWorkgroupMetadata(), Task.class));
                        break;
                }
            }
        }


    }

    /**
     * Sends broadcast to notify about data update
     * @param value type of notification as {@link ServiceResult} enum value
     */
    private void sendUpdateBroadcast(ServiceResult value){
        Intent broadcastIntent = new Intent(Consts.UPDATE_BROADCAST_TAG);
        broadcastIntent.putExtra(Consts.STATUS_EXTRA, value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        stopSelf();
    }
}
