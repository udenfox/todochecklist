package xyz.spacefox.todochecklist;

import android.app.Application;

import xyz.spacefox.todochecklist.utils.Database;

/**
 * Created by Denis on 26.02.2016.
 */
public class ToDoApplication extends Application {

    private Database mDatabaseHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        mDatabaseHelper = new Database(getApplicationContext());
        mDatabaseHelper.open();
    }

    public Database getDatabaseHelper(){
        return mDatabaseHelper;
    }
}
