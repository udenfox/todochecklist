package xyz.spacefox.todochecklist.utils;

/**
 * Class that contains main project constants
 */
public final class Consts {

    public static final String API_URL = "https://auth.simplexsolutionsinc.com";
    public static final String SSL_URI = "https://rpc.v1.keepsolid.com:443";
    public static  final int HTTP_STATUS_OK = 200;

    public static final String LOGIN_ACTION = "login";
    public static final String LOGIN_SERVICE = "com.braininstock.ToDoChecklist";
    public static final String PLATFORM = "Android";

    public static final String PREFS_NAME = "xyz.spacefox.ToDoChecklist.PREFS";
    public static final String PREFS_SESSIONID = "sessionId";
    public static final String PREFS_LOGIN = "userLogin";

    public static final int LOADER_TASKLISTS = 1;
    public static final int LOADER_TASKS = 0;

    public static final String ALL_TASKS_ALIAS = "local_all";
    public static final String NO_LIST_TASKS_ALIAS = "local_no_list";

    public static final String STATE_CURRENT_ALIAS = "current_alias";

    public static final String UPDATE_BROADCAST_TAG = "xyz.spacefox.ToDoChecklist.UPDATED";
    public static final String STATUS_EXTRA = "ServiceStatus";

    public static final String LATLNG_ARG_TAG = "position_lat_lng";
    public static final String RADIUS_ARG_TAG = "position_radius";
    public static final String MAP_DIALOG_TAG = "map_dialog";

    public static final String TASK_ALIAS_EXTRA = "task_alias";

    public static final int TYPE_LIST = 1002;
    public static final int TYPE_TASK = 1001;

}
