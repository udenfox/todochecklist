package xyz.spacefox.todochecklist.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import xyz.spacefox.todochecklist.api.model.Task;
import xyz.spacefox.todochecklist.api.model.TaskList;

/**
 * Class to simplify interaction with the database. Contains specific methods to interact
 * with database depends on project's needs.
 */
public class Database {

    private final Context mCtx;

    // Custom DbHelper.
    private DBHelper mDBHelper;

    // Database itself
    private SQLiteDatabase mDB;

    /**
     * Constructor of database object.
     *
     * @param ctx Context for database.
     */
    public Database(Context ctx) {
        mCtx = ctx;
    }

    /**
     * Opens a connection to database
     */
    public void open() {
        mDBHelper = new DBHelper(mCtx, DBConsts.DB_NAME, null, DBConsts.DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     * Closes the connection to database.
     */
    public void close() {
        if (mDBHelper != null) mDBHelper.close();
    }

    /**
     * Retrieves tasks from specified task list
     * @param taskListAlias alias of task list to get tasks from
     * @return Cursor containing task from specified tasklist or all tasks with list name,
     * if no tasklist specified by alias
     */
    public Cursor getTasks(String taskListAlias) {

        if (taskListAlias == null || taskListAlias.equals("") || taskListAlias.equals(Consts.ALL_TASKS_ALIAS)) {
            return mDB.query(DBConsts.TABLE_TASKS, null, null, null, null, null, null);
        } else if(taskListAlias.equals(Consts.NO_LIST_TASKS_ALIAS)) {
            return mDB.rawQuery(DBConsts.GET_NO_LIST_TASKS, null);
        } else{
            String query = String.format(DBConsts.GET_TASKS_FROM_TASKLIST, taskListAlias);
            return mDB.rawQuery(query, null);
        }
    }

    /**
     * Retrieves Task with specified alias from database if availible
     * @param alias alias of task to get from database
     * @return Task with specified alias or null
     */
    public Task getTaskByAlias (String alias) {
        Cursor c = mDB.query(DBConsts.TABLE_TASKS, null,
                DBConsts.COL_TASK_ALIAS + "=" + "'" + alias + "'",
                null, null, null, null);

        Task task = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            task = new Task(c);
        }
        c.close();
        return task;
    }

    /**
     * Retrieves tasklist title for a specified task if available
     * @param taskAlias alias of task to get list title for
     * @return title of list which contain task with specified alias or null
     */
    public String getListTitleByTask(String taskAlias){
        String query = String.format(DBConsts.GET_LIST_NAME_BY_TASK, taskAlias);
        Cursor c = mDB.rawQuery(query, null, null);

        String listTitle = null;
        if (c.getCount() > 0) {
            c.moveToFirst();
            listTitle = c.getString(c.getColumnIndex(DBConsts.COL_LIST_TITLE));
        }
        c.close();
        return  listTitle;
    }

    /**
     * Retrieves all task lists
     */
    public Cursor getTasksLists() {
        return mDB.query(DBConsts.TABLE_TASKLISTS, null, null, null, null, null, null);
    }

    /**
     * Add {@link Task} item to a database
     */
    public void addTask(Task task) {

        ContentValues cv = new ContentValues();
        cv.put(DBConsts.COL_TASK_ALIAS, task.getAlias());
        cv.put(DBConsts.COL_TASK_TITLE, task.getTitle());
        cv.put(DBConsts.COL_NOTES, task.getNotes());
        if (task.getRemindOnDate() != null) {
            cv.put(DBConsts.COL_REMIND_ON_DATE, task.getRemindOnDate().getTime());
        }
        cv.put(DBConsts.COL_INVITEE, task.getInvitee());
        if (task.getRemindOnLocation().getCoordinates() != null){
            cv.put(DBConsts.COL_REMIND_LAT, task.getRemindOnLocation().getCoordinates().latitude);
            cv.put(DBConsts.COL_REMIND_LNG, task.getRemindOnLocation().getCoordinates().longitude);
        }
        cv.put(DBConsts.COL_REMIND_DESCRIPTION, task.getRemindOnLocation().getDescription());
        cv.put(DBConsts.COL_REMIND_ON_ENTER, task.getRemindOnLocation().getRemindOnEnter());
        cv.put(DBConsts.COL_REMIND_RADIUS, task.getRemindOnLocation().getRadius());
        cv.put(DBConsts.COL_VERSION, task.getVersion());
        if (task.getCreationDate() != null) {
            cv.put(DBConsts.COL_CREATION_DATE, task.getCreationDate().getTime());
        }
        cv.put(DBConsts.COL_COMPLETED, task.getCompleted());

        mDB.insert(DBConsts.TABLE_TASKS, null, cv);

    }

    /**
     * Add {@link TaskList} item to a database
     */
    public void addTaskList(TaskList tasklist) {

        ContentValues cv = new ContentValues();
        cv.put(DBConsts.COL_LIST_ALIAS, tasklist.getAlias());
        cv.put(DBConsts.COL_LIST_TITLE, tasklist.getTitle());

        mDB.insert(DBConsts.TABLE_TASKLISTS, null, cv);

        for(String taskAlias : tasklist.getTasks()){
            ContentValues cvTasks = new ContentValues();
            cvTasks.put(DBConsts.COL_TASK_ALIAS, taskAlias);
            cvTasks.put(DBConsts.COL_LIST_ALIAS, tasklist.getAlias());

            mDB.insert(DBConsts.TABLE_LIST_TASKS, null, cvTasks);
        }

    }

    /**
     * Delete all data from a database
     */
    public void clearDatabase() {
        mDB.delete(DBConsts.TABLE_LIST_TASKS, null, null);
        mDB.rawQuery(DBConsts.DELETE_NON_LOCAL_LISTS, null);
        mDB.delete(DBConsts.TABLE_TASKS, null, null);
    }

    /**
     * Subclass of {@link SQLiteOpenHelper} which provides custom database helper.
     */
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DBConsts.CREATE_TASKS_TABLE);
            db.execSQL(DBConsts.CREATE_TASKLISTS_TABLE);
            db.execSQL(DBConsts.CREATE_LIST_TASKS_TABLE);
            db.execSQL(DBConsts.INSERT_ALL_TASKLIST_ENTRY);
            db.execSQL(DBConsts.INSERT_NO_LIST_TASKLIST_ENTRY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DBConsts.DROP_TASKS);
            db.execSQL(DBConsts.DROP_TASKLISTS);
            db.execSQL(DBConsts.DROP_LIST_TASKS);
            onCreate(db);
        }
    }
}

