package xyz.spacefox.todochecklist.utils;

/**
 * Class that contains constants used to manage project database
 */
public final class DBConsts {

    public static final String DB_NAME = "toDo_db";
    public static final int DB_VERSION = 1;
    public static final int ALL_LIST_ID = 0;
    public static final int NO_LIST_ID = 1;
    public static final String TABLE_TASKLISTS = "task_lists";
    public static final String TABLE_TASKS = "tasks";
    public static final String TABLE_LIST_TASKS = "tasklist_task";

    public static final String COL_ID = "_id";
    public static final String COL_TASK_TITLE = "title";
    public static final String COL_LIST_TITLE = "list_title";

    // List_tasks
    public static final String COL_LIST_ALIAS = "list_alias";
    public static final String COL_TASK_ALIAS = "task_alias";

    // Tasks
    public static final String COL_NOTES = "notes";
    public static final String COL_REMIND_ON_DATE = "remind_on_date";
    public static final String COL_INVITEE = "invitee";
    public static final String COL_REMIND_LAT = "remind_lat";
    public static final String COL_REMIND_LNG = "remind_lng";
    public static final String COL_REMIND_ON_ENTER = "remind_on_enter";
    public static final String COL_REMIND_DESCRIPTION = "remind_description";
    public static final String COL_REMIND_RADIUS = "remind_radius";
    public static final String COL_VERSION = "version";
    public static final String COL_CREATION_DATE = "creation_date";
    public static final String COL_COMPLETED = "completed";

    // Query
    public static final String CREATE_TASKS_TABLE =
            "create table " + TABLE_TASKS + "(" +
                    COL_ID + " integer primary key, " +
                    COL_TASK_ALIAS + " text, " +
                    COL_TASK_TITLE + " text, " +
                    COL_NOTES + " text, " +
                    COL_REMIND_ON_DATE + " integer, " +
                    COL_INVITEE + " text, " +
                    COL_REMIND_LAT + " real, " +
                    COL_REMIND_LNG + " real, " +
                    COL_REMIND_ON_ENTER + " integer, " +
                    COL_REMIND_DESCRIPTION + " text, " +
                    COL_REMIND_RADIUS + " integer, " +
                    COL_VERSION + " text, " +
                    COL_CREATION_DATE + " integer, " +
                    COL_COMPLETED + " integer, " +
                    " UNIQUE ( " + COL_TASK_ALIAS + " ) ON CONFLICT REPLACE);";

    public static final String CREATE_LIST_TASKS_TABLE =
            "create table " + TABLE_LIST_TASKS + "(" +
                    COL_TASK_ALIAS + " text, " +
                    COL_LIST_ALIAS + " text " +
                    ");";

    public static final String CREATE_TASKLISTS_TABLE =
            "create table " + TABLE_TASKLISTS + "(" +
                    COL_ID + " integer primary key, " +
                    COL_LIST_ALIAS + " text, " +
                    COL_LIST_TITLE + " text, " +
                    " UNIQUE ( " + COL_LIST_ALIAS + " ) ON CONFLICT REPLACE);";

    public final static String DROP_TASKLISTS =
            "DROP TABLE IF EXISTS " + TABLE_TASKLISTS;
    public final static String DROP_LIST_TASKS =
            "DROP TABLE IF EXISTS " + TABLE_LIST_TASKS;
    public final static String DROP_TASKS =
            "DROP TABLE IF EXISTS " + TABLE_TASKS;

    public static final String GET_TASKS_FROM_TASKLIST =
            "SELECT * FROM " + TABLE_TASKS+ " LEFT JOIN " + TABLE_LIST_TASKS +
                    " USING (" + COL_TASK_ALIAS  + ")" +
                    " WHERE (" + COL_LIST_ALIAS + " = '%s');";

    public static final String GET_TASKS_INCLUDE_LIST_ALIAS =
            "SELECT * FROM " + TABLE_TASKS + " LEFT JOIN " + TABLE_LIST_TASKS +
                    " USING (" + COL_TASK_ALIAS + ")";

    public static final String GET_NO_LIST_TASKS =
            "SELECT * FROM (" + GET_TASKS_INCLUDE_LIST_ALIAS + ")" +
                    " WHERE (" + COL_LIST_ALIAS + " IS NULL);";

    public static final String GET_LIST_NAME_BY_TASK =
            "SELECT " + COL_LIST_TITLE + " FROM " + TABLE_TASKLISTS +
                    " LEFT JOIN " + TABLE_LIST_TASKS + " USING (" + COL_LIST_ALIAS + ") "+
                    "WHERE (" + COL_TASK_ALIAS + "='%s');";

    public static final String INSERT_ALL_TASKLIST_ENTRY =
            "INSERT INTO " +
                    TABLE_TASKLISTS + " (" + COL_LIST_ALIAS + ") " +
                    "VALUES ('" +Consts.ALL_TASKS_ALIAS+ "');";

    public static final String INSERT_NO_LIST_TASKLIST_ENTRY =
            "INSERT INTO " +
                    TABLE_TASKLISTS + " (" + COL_LIST_ALIAS + ") " +
                    "VALUES ('" +Consts.NO_LIST_TASKS_ALIAS+ "');";

    public static final String DELETE_NON_LOCAL_LISTS =
            "DELETE FROM " + TABLE_TASKLISTS + " WHERE " + COL_LIST_ALIAS +
                    " NOT IN ('" + Consts.ALL_TASKS_ALIAS + "', '" + Consts.NO_LIST_TASKS_ALIAS + "')";
}
