package xyz.spacefox.todochecklist;

import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import xyz.spacefox.todochecklist.adapters.NavigationCursorAdapter;
import xyz.spacefox.todochecklist.adapters.TaskListCursorAdapter;
import xyz.spacefox.todochecklist.api.model.TaskList;
import xyz.spacefox.todochecklist.service.ApiCallService;
import xyz.spacefox.todochecklist.service.ServiceResult;
import xyz.spacefox.todochecklist.utils.Consts;
import xyz.spacefox.todochecklist.utils.DBConsts;
import xyz.spacefox.todochecklist.utils.Database;

/**
 * Main activity: Shows tasklist to user.
 * Uses Navigation drawer to change task lists and swipe-to-refresh layout to data refresh.
 *
 * Implements {@link android.app.LoaderManager.LoaderCallbacks} to get connection with database.
 * Implements {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener} to handle
 * swipe-to-refresh actions.
 */
public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private ListView mMainList;
    private TextView mListEmptyText;
    private SwipeRefreshLayout mRefreshLayout;

    private ActionBarDrawerToggle mDrawerToggle;
    private Database mDb;
    private NavigationCursorAdapter mNavigationAdapter;
    private TaskListCursorAdapter mTasksAdapter;
    private Loader mTasksLoader;
    private Loader mDrawerLoader;
    private BroadcastReceiver mServiceReceiver;

    private String mListAlias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();

        mDb = ((ToDoApplication) getApplication()).getDatabaseHelper();

        // Initialize swipe-to-refresh layout
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRefreshLayout.setOnRefreshListener(this);

        // Initialize main list and it's adapter
        mMainList = (ListView) findViewById(R.id.main_list);
        mListEmptyText = (TextView) findViewById(R.id.main_list_empty);
        mTasksAdapter = new TaskListCursorAdapter(this, null, false);
        mMainList.setAdapter(mTasksAdapter);
        mMainList.setEmptyView(mListEmptyText);

        // Initialize navigation drawer accessories
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.primary_dark));
        mDrawerToggle = new ActionBarDrawerToggle( this, mDrawerLayout, mToolbar,
                R.string.drawer_open_content_descr, R.string.drawer_close_content_descr);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        // Initialize navigation drawer list
        mDrawerListView = (ListView) findViewById(R.id.left_drawer);
        setHeader();
        mNavigationAdapter = new NavigationCursorAdapter(this, null, false);
        mDrawerListView.setAdapter(mNavigationAdapter);

        // Try to get selected list alias from saved state bundle
        if (savedInstanceState != null) {
            mListAlias = savedInstanceState.getString(Consts.STATE_CURRENT_ALIAS);
        } else {
            mListAlias = Consts.ALL_TASKS_ALIAS;
        }

        mDrawerLoader = getLoaderManager().initLoader(Consts.LOADER_TASKLISTS, null, this);
        mTasksLoader = getLoaderManager().initLoader(Consts.LOADER_TASKS, null, this);


        mDrawerListView.setOnItemClickListener((parent, view, position, id) -> {
            mDrawerLayout.closeDrawers();
            TaskList selectedItem = mNavigationAdapter.getItem(position-1);

            // Set selected item title as activity title and save alias
            mListAlias = selectedItem.getAlias();
            mToolbar.setTitle(selectedItem.getTitle());
            getLoaderManager().restartLoader(Consts.LOADER_TASKS, null, this);
        });

        mMainList.setOnItemClickListener((parent, view, position, id) -> {
            Intent detailIntent = new Intent(MainActivity.this, DetailActivity.class);
            detailIntent.putExtra(Consts.TASK_ALIAS_EXTRA,
                    mTasksAdapter.getItem(position).getAlias());
            startActivity(detailIntent);
        });

        // Receiver to handle broadcasts from ApiCallService
        mServiceReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServiceResult result = (ServiceResult) intent.getSerializableExtra(Consts.STATUS_EXTRA);

                if(result == ServiceResult.UPDATE){
                    mTasksLoader.forceLoad();
                    mDrawerLoader.forceLoad();
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.error_network),
                            Toast.LENGTH_SHORT).show();
                }

                setRefreshState(false);

            }
        };

        // Try to refresh data if it's activity first start
        if (savedInstanceState == null) {
            this.onRefresh();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        // Receiver to obtain broadcasts from ApiCallService
        LocalBroadcastManager.getInstance(MainActivity.this)
                .registerReceiver(mServiceReceiver, new IntentFilter(Consts.UPDATE_BROADCAST_TAG));
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unregister update receiver
        mRefreshLayout.setRefreshing(false);
        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(mServiceReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save selected tasklist alias
        outState.putString(Consts.STATE_CURRENT_ALIAS, mListAlias);
    }

    @Override
    protected void onDestroy() {
        getLoaderManager().destroyLoader(Consts.LOADER_TASKLISTS);
        getLoaderManager().destroyLoader(Consts.LOADER_TASKS);
        super.onDestroy();
    }

    /**
     * Starts {@link ApiCallService} to retrieve data from server
     */
    private void getData(){
        setRefreshState(true);
        Intent i = new Intent(MainActivity.this, ApiCallService.class);
        startService(i);
    }

    /**
     * Configures and sets header view for navigationDrawer listview
     */
    private void setHeader(){

        String userLogin = getSharedPreferences(Consts.PREFS_NAME, MODE_PRIVATE)
                .getString(Consts.PREFS_LOGIN, null);

        View v = getLayoutInflater().inflate(R.layout.nav_drawer_header, mDrawerListView, false);
        TextView userTextPrimary = (TextView) v.findViewById(R.id.ndr_header_user_primary);
        if (userLogin != null && !userLogin.equals("")) {
            userTextPrimary.setText(userLogin);
        }

        mDrawerListView.addHeaderView(v);
    }

    /**
     * Retrieves a position of the Tasklist item with specified alias in a specified cursor.
     * @param alias Alias of the Tasklist item to get position of.
     * @param c Cursor whose position is obtained
     * @return position of item in cursor (count start from 1)
     */
    private int getAliasCursorPosition(String alias, Cursor c) {
        int i = 0;
        if (alias == null) {
            return 1;
        }
        while (c.moveToNext()) {
            i++;
            String currentAlias = c.getString(c.getColumnIndex(DBConsts.COL_LIST_ALIAS));
            if (alias.equals(currentAlias)) {
                break;
            }
        }
        return i;
    }

    /** Initializes the toolbar. */
    private void initToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * Sets refresh status with changing list empty view text
     * @param state refreshing state
     */
    private void setRefreshState(boolean state) {
        if (state) {
           mListEmptyText.setText(getString(R.string.main_list_empty_view_loading));
        } else {
            mListEmptyText.setText(getString(R.string.main_list_empty_view));
        }
        mRefreshLayout.setRefreshing(state);
    }

    @Override
    public void onRefresh() {
        getData();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case Consts.LOADER_TASKS:
                return new MyCursorLoader(this, mDb, mListAlias);
            case Consts.LOADER_TASKLISTS:
                return new MyCursorLoader(this, mDb);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case Consts.LOADER_TASKS:
                mTasksAdapter.swapCursor(data);
                break;
            case Consts.LOADER_TASKLISTS:
                mNavigationAdapter.swapCursor(data);
                int selectedPosition = getAliasCursorPosition(mListAlias, data);

                // Check right navigationDrawer item and set it's name as title
                mDrawerListView.setItemChecked(selectedPosition, true);
                mToolbar.setTitle(mNavigationAdapter.getItem(selectedPosition-1).getTitle());
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Subclass of {@link android.content.CursorLoader} which provides loader associated
     * with application database's implementation.
     */
    static class MyCursorLoader extends CursorLoader {

        Database db;
        int loaderId;
        String listAlias;

        /**
         * Constructs CursorLoader to load taskLists from a database
         */
        public MyCursorLoader(Context context, Database db) {
            super(context);
            this.db = db;
            this.loaderId = Consts.LOADER_TASKLISTS;
        }

        /**
         * Constructs CursorLoader to load tasks from a database
         * @param listAlias alias of tasklist to load tasks from
         */
        public MyCursorLoader(Context context, Database db, String listAlias) {
            super(context);
            this.db = db;
            this.loaderId = Consts.LOADER_TASKS;
            this.listAlias = listAlias;
        }

        @Override
        public Cursor loadInBackground() {

            switch (loaderId) {
                case Consts.LOADER_TASKLISTS:
                    return db.getTasksLists();
                case Consts.LOADER_TASKS:
                    return db.getTasks(listAlias);
                default:
                    return null;
            }

        }

    }

}
