package xyz.spacefox.todochecklist;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import xyz.spacefox.todochecklist.utils.Consts;

/**
 * Dialog fragment showing map zoomed to specified location and radius.
 */
public class MapDialogFragment extends DialogFragment {

    private GoogleMap mMap;
    private LatLng mPosition;
    private int mRadius;
    private SupportMapFragment mMapFragment;

    /**
     * Retrieves new dialog instance with specified parameters
     * @param position position to set on map
     * @param radius radius to set on map
     * @return new {@link MapDialogFragment} with setted arguments
     */
    public static MapDialogFragment newInstance(LatLng position, int radius) {
        MapDialogFragment m = new MapDialogFragment();

        Bundle args = new Bundle();
        args.putParcelable(Consts.LATLNG_ARG_TAG, position);
        args.putInt(Consts.RADIUS_ARG_TAG, radius);
        m.setArguments(args);

        return m;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_dialog, container);
        mPosition = getArguments().getParcelable(Consts.LATLNG_ARG_TAG);
        mRadius = getArguments().getInt(Consts.RADIUS_ARG_TAG);
        mMapFragment = new SupportMapFragment();

        getDialog().requestWindowFeature(STYLE_NO_TITLE);

        getChildFragmentManager().beginTransaction().add(R.id.map_view, mMapFragment).commit();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setDialogAppearance();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeMap();

    }

    /**
     * Sets dialog layout parameters for proper appearance
     */
    private void setDialogAppearance() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int toolbarSizePx = getActivity().getResources().getDimensionPixelSize(R.dimen.toolbar_height);
        int dialogHeight = size.y - (toolbarSizePx + (toolbarSizePx/2));

        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, dialogHeight);
    }

    /**
     * Initializes map and sets marker to it
     */
    private void initializeMap() {

        if (mMapFragment != null) {

            mMapFragment.getMapAsync(googleMap -> {
                mMap = googleMap;
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setTiltGesturesEnabled(false);
                setMapPosition();
           });

        }
    }

    /**
     * Sets marker to a map and zooms to that marker
     */
    private void setMapPosition(){
        mMap.clear();
        if(mPosition != null){
            addMarkerWithRadius(mPosition, mRadius);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mPosition, getZoomLevel(mRadius)));
        }

    }

    /**
     * Adds marker and radius circle to a map
     * @param position position of a marker to set
     * @param radius radius in meters to draw radius circle
     */
    private void addMarkerWithRadius(LatLng position, int radius){
        mMap.addCircle(new CircleOptions()
                .center(position)
                .radius(radius)
                .strokeColor(ContextCompat.getColor(getActivity(), R.color.map_circle_stroke))
                .strokeWidth(getActivity().getResources().getDimensionPixelSize(R.dimen.map_circle_stroke_width))
                .fillColor(ContextCompat.getColor(getActivity(), R.color.map_circle)));

        MarkerOptions markerOptions = new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_36dp))
                .anchor(0.5f, 1f);
        mMap.addMarker(markerOptions);
    }

    /**
     * Retrieves comfortable zoom level for specified radius
     * @param radius radius to calculate zoom level for
     * @return zoom level value
     */
    public int getZoomLevel(int radius) {
        double scale = (double) radius / 500;
        int zoomLevel =(int) (16 - Math.log(scale) / Math.log(2));
        return zoomLevel-2;
    }


}
