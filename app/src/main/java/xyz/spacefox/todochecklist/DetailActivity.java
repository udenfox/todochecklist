package xyz.spacefox.todochecklist;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import xyz.spacefox.todochecklist.api.model.Task;
import xyz.spacefox.todochecklist.utils.Consts;
import xyz.spacefox.todochecklist.utils.Database;

/**
 * Detailed activity: Shows task details.
 * Obtains task Alias using intent and loads it from database asynchronously.
 */
public class DetailActivity extends AppCompatActivity {

    private ViewGroup mContentView;
    private TextView mTitleText;
    private CheckBox mCheckBox;
    private Toolbar mToolbar;
    private MapDialogFragment mMapDialogFragment;

    private Task mTask;
    private String mTaskListTitle;
    private String mTaskAlias;

    private Database mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mDb = ((ToDoApplication) getApplication()).getDatabaseHelper();;

        initToolbar();

        mContentView = (ViewGroup) findViewById(R.id.detail_content_view);
        mTitleText = (TextView) findViewById(R.id.toolbar_title_text);
        mCheckBox = (CheckBox) findViewById(R.id.toolbar_title_check);

        mTaskAlias = getIntent().getStringExtra(Consts.TASK_ALIAS_EXTRA);

        new LoadTask().execute(mTaskAlias);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    /** Initializes the toolbar. */
    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /**
     * Creates necessary view blocks according to the displayed task info.
     */
    private void populateContent() {

        Date remindOnDate = mTask.getRemindOnDate();
        String invitee = mTask.getInvitee();
        String locationText = mTask.getRemindOnLocation().getDescription();
        String taskTitle = mTask.getTitle();
        String taskNotes = mTask.getNotes();

        mCheckBox.setChecked(mTask.getCompleted());
        mTitleText.setText(taskTitle);

        if (mTaskListTitle != null && !mTaskListTitle.equals("")) {
            addDetailBlock(mTaskListTitle, ItemType.LIST);
        }

        if (taskNotes != null && !taskNotes.equals("")) {
            addDetailBlock(taskNotes, ItemType.NOTE);
        }

        if (remindOnDate != null) {
            addDetailBlock(SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT,
                            DateFormat.SHORT).format(remindOnDate), ItemType.NOTIFY_ON);
        }

        if (invitee != null && !invitee.equals("")) {
            addDetailBlock(invitee, ItemType.SHARED);
        }

        if (locationText != null && !locationText.equals("")) {
            mMapDialogFragment = MapDialogFragment
                    .newInstance(mTask.getRemindOnLocation().getCoordinates(),
                            mTask.getRemindOnLocation().getRadius());
            addDetailBlock(locationText, ItemType.POSITION);
            addLocationBlock(mTask.getRemindOnLocation().getRemindOnEnter());
        }


    }

    /**
     * Adds info block to the activity layout with specified value and type
     * @param value value of the added block
     * @param type block type
     */
    private void addDetailBlock(String value, ItemType type){
        View v = LayoutInflater.from(this).inflate(R.layout.detailed_item, mContentView, false);
        TextView itemTitle = (TextView) v.findViewById(R.id.detailed_item_title);
        TextView itemValue = (TextView) v.findViewById(R.id.detailed_item_value);
        ImageView itemIcon = (ImageView) v.findViewById(R.id.detailed_item_icon);

        int iconDrawableId = -1;
        String titleText = "";

        // Select block icon and title according to block type
        switch (type) {
            case NOTIFY_ON:
                iconDrawableId = R.drawable.ic_alarm_grey600_24dp;
                titleText = getString(R.string.notify_on_title);
                break;
            case SHARED:
                iconDrawableId = R.drawable.ic_account_multiple_grey600_24dp;
                titleText = getString(R.string.shared_with_title);
                break;
            case POSITION:
                iconDrawableId = R.drawable.ic_map_marker_radius_grey600_24dp;
                titleText = getString(R.string.position_title);
                break;
            case NOTE:
                iconDrawableId = R.drawable.ic_note_outline_grey600_24dp;
                titleText = getString(R.string.notes_title);
                break;
            case LIST:
                iconDrawableId = R.drawable.ic_view_list_grey600_24dp;
                titleText = getString(R.string.list_title);
                break;
        }

        // Set block icon, value and title
        itemTitle.setText(titleText);
        itemValue.setText(value);
        if( iconDrawableId != -1 ){
            itemIcon.setImageDrawable(ContextCompat.getDrawable(this, iconDrawableId));
        }

        mContentView.addView(v);


    }

    /**
     * Adds special location info block to the activity layout. This block contains "Notify on enter"
     * switch indicator and clickable layout to show the location on a map.
     * @param notifyOnEnter check for notify on enter option.
     */
    private void addLocationBlock(boolean notifyOnEnter) {

        View v = LayoutInflater.from(this).inflate(R.layout.detailed_geo_item, mContentView, false);
        SwitchCompat mNotifyOnEnterCheck = (SwitchCompat) v.findViewById(R.id.notify_on_enter_check);
        LinearLayout mShowOnMap = (LinearLayout) v.findViewById(R.id.show_on_map_block);

        mNotifyOnEnterCheck.setChecked(notifyOnEnter);
        mShowOnMap.setOnClickListener(v1 -> {

            mMapDialogFragment.show(getSupportFragmentManager(), Consts.MAP_DIALOG_TAG);
        });

        mContentView.addView(v);
    }

    /**
     * Enum populates values to differentiate a detailed info block types
     */
    private enum ItemType {
        NOTIFY_ON, SHARED, POSITION, NOTE, LIST
    }

    /**
     * {@link AsyncTask} to load data from the database
     */
    private class LoadTask extends AsyncTask<String, Void, Task> {

        @Override
        protected Task doInBackground(String... params) {
            String alias = params[0];
            mTaskListTitle = mDb.getListTitleByTask(mTaskAlias);
            return mDb.getTaskByAlias(alias);
        }

        @Override
        protected void onPostExecute(Task task) {
            super.onPostExecute(task);
            mTask = task;
            populateContent();

        }
    }
}
