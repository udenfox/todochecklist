# About App #
To-do checklist viewer

# Key features #
* Google Maps Android API used
* Data stored locally in a SQLite database
* Android Service used to manage API updates
* Navigation drawer implemented
* UI developed according to Material Design guidelines
* REST API interaction using Retrofit
* Protocol buffers used to interact with RAW SSL API
* RAW SSL API client developed

[**Download APK**](https://bitbucket.org/udenfox/todochecklist/raw/77cc1e3d30fe778b60fc7cdb0cac741401c8c40b/app-debug.apk)